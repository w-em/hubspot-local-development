const path = require("path");

module.exports = {
  baseDir: __dirname,
  hubspot: path.join(__dirname, 'nunjucks', 'hubspot'),
  final: path.join(__dirname, 'final'),
  src: {
    baseDir: path.join(__dirname, 'src'),
    templateDir: path.join(__dirname, 'src', 'templates'),
    moduleDir: path.join(__dirname, 'src', 'modules'),
    jsDir: path.join(__dirname, 'src', 'js'),
    cssDir: path.join(__dirname, 'src', 'css'),
    imageDir: path.join(__dirname, 'src', 'images'),
  },
  output: {
    baseDir: path.join(__dirname, 'dist'),
    cssDir: path.join(__dirname, 'dist', 'css'),
    jsDir: path.join(__dirname, 'dist', 'js'),
    imageDir: path.join(__dirname, 'dist', 'images'),
  }
}
