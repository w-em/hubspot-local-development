const gulp = require("gulp"),
  changed = require("gulp-changed"),
  path = require("path"),
  plumber = require("gulp-plumber"),
  sourcemaps = require("gulp-sourcemaps"),
  beautify = require("gulp-jsbeautifier"),
  browserSync = require("browser-sync");

const config = require("./../config");

module.exports = {
  final: function () {
    return gulp
      .src([
        path.join(config.src.baseDir, '**/*'),
      ], {
        dot: true // include hidden files
      })
      .pipe(changed(config.final))
      .pipe(gulp.dest(config.final));
  }
}
