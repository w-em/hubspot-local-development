var browserSync = require('browser-sync');

module.exports = function () {
  return browserSync({
    port: 9000,
    notify: true,
    server: {
      baseDir: ['dist']
    }
  });
};
