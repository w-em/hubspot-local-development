const path = require("path"),
  gulp = require("gulp"),
  plumber = require("gulp-plumber"),
  notify = require("gulp-notify"),
  sourcemaps = require("gulp-sourcemaps"),
  beautify = require('gulp-jsbeautifier'),
  browserSync = require("browser-sync");

const config = require('./../config');
const onError = function(err) {
  notify.onError({
    title: "Gulp error in " + err.plugin,
    message: err.toString()
  })(err);
};

module.exports = {
  main: function () {
    return gulp
      .src([path.join(config.src.jsDir, '*.js')])
      .pipe(plumber({ errorHandler: onError }))
      .pipe(sourcemaps.init())
      .pipe(beautify())
      .pipe(gulp.dest(config.output.jsDir))
      .pipe(browserSync.reload({ stream: true }));

  },
  modules: function () {
      return gulp
        .src([path.join(config.src.moduleDir, '**/*.js')])
        .pipe(plumber({ errorHandler: onError }))
        .pipe(sourcemaps.init())
        .pipe(beautify())
        .pipe(gulp.dest(config.output.jsDir))
        .pipe(browserSync.reload({ stream: true }));

  }
}
