const gulp = require("gulp"),
  del = require("del"),
  browserSync = require("browser-sync");

const config = require("./../config");
const path = require("path");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const beautify = require("gulp-jsbeautifier");

module.exports = {
  dest: async function () {
    return del.sync(config.output.baseDir);
  },
  final: async function () {
    return del.sync(config.final);
  }
}
