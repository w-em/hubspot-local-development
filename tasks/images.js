const gulp = require("gulp"),
  changed = require("gulp-changed"),
  path = require("path"),
  plumber = require("gulp-plumber"),
  sourcemaps = require("gulp-sourcemaps"),
  beautify = require("gulp-jsbeautifier"),
  browserSync = require("browser-sync");

const config = require("./../config");

module.exports = function () {
  return gulp
    .src([path.join(config.src.imageDir, '**/*')], {
      dot: true // include hidden files
    })
    .pipe(changed(config.output.imageDir))
    .pipe(gulp.dest(config.output.imageDir))
    .pipe(browserSync.reload({ stream: true }));
}
