// Renders Nunjucks
const gulp = require("gulp");
const gulpNunjucks = require('gulp-nunjucks');
const nunjucks = require('nunjucks');
const path = require("path");
const { readdirSync, readFileSync } = require('fs-extra');
const browserSync = require("browser-sync");
const rename = require('gulp-rename');
const htmlbeautify = require('gulp-html-beautify');
const cheerio = require('gulp-cheerio');
const jsbeautify = require('gulp-jsbeautifier');

const config = require('./../config');
const { getFieldDefaults, getUsedFonts } = require("../nunjucks/helpers");
const htmlBeautifySettings = {
  "indent_size": 4,
  "indent_char": " ",
  "eol": "\n",
  "indent_level": 0,
  "indent_with_tabs": true,
  "preserve_newlines": false,
  "max_preserve_newlines": 10,
  "jslint_happy": false,
  "space_after_anon_function": false,
  "brace_style": "collapse",
  "keep_array_indentation": false,
  "keep_function_indentation": false,
  "space_before_conditional": true,
  "break_chained_methods": false,
  "eval_code": false,
  "unescape_strings": false,
  "wrap_line_length": 0,
  "wrap_attributes": "auto",
  "wrap_attributes_indent_size": 4,
  "end_with_newline": false
};

const postGeneratingTemplate = function($, file) {
  let moduleCss = [], blockCss = [];
  let moduleJs = [];
  let moduleName = '', blockName = '',
    insertAt;
  let position = 'head';
  const head = $('head');
  const body = $('body');

  // Each file will be run through cheerio and each corresponding `$` will be passed here.
  // `file` is the gulp file object
  // Make all h1 tags uppercase
  $('body [data-module-css]').each(function() {
    var self = $(this);
    moduleName = self.attr('data-module-css');
    if (moduleCss.indexOf(moduleName) === -1) {
      position = self.attr("data-position") || 'head';
      switch (position) {
        case 'footer': insertAt = body; break;
        default: insertAt = head; break;
      }

      insertAt.append('<link rel="stylesheet" type="text/css" href="css/'+moduleName+'/module.css" />');
      moduleCss.push(moduleName);
    }
    self.remove();
  });

  $('body [data-module-js]').each(function() {
    var self = $(this);
    moduleName = self.attr('data-module-js');
    if (moduleJs.indexOf(moduleName) === -1) {
      position = self.attr("data-position") || 'head';
      switch (position) {
        case 'footer': insertAt = body; break;
        default: insertAt = head; break;
      }

      insertAt.append('<script src="js/'+moduleName+'/module.js"></script>');
      moduleJs.push(moduleName);
    }
    self.remove();
  });

  $('body [data-require_css]').each(function() {
    var self = $(this);
    head.append('<style>' + self.text() + '</style>');
    self.remove();
  });

  $('body [data-block-css]').each(function() {
    var self = $(this);
    blockName = self.attr('data-block-css');
    if (blockCss.indexOf(blockName) === -1) {
      head.append('<style>' + self.text() + '</style>');
      blockCss.push(blockName);
    }
    self.remove();
  });

}
const fieldFile = path.join(config.src.baseDir, 'fields.json');
const fieldJsonData = JSON.parse(readFileSync(fieldFile, 'utf8'));
const fieldJson = getFieldDefaults(fieldJsonData);


module.exports = {
  templates: function () {
    // Gets .html files in templates
    const env = require(path.join(__dirname, '..', 'env.js'))(config.src.templateDir, true);
    return gulp
      .src(path.join(config.src.templateDir, '*.+(html|njk)'))
      .pipe(gulpNunjucks.compile({
        theme: fieldJson
      }, {
        env: env
      }))
      .pipe(
        cheerio(postGeneratingTemplate),
      )
      // output files in app folder
      .pipe(htmlbeautify(htmlBeautifySettings))
      .pipe(gulp.dest(config.output.baseDir))
      .pipe(browserSync.reload({ stream: true }));
  },
  css: function () {
    // Gets .css files in templates
    // fields.json



    return gulp
      .src(path.join(config.src.cssDir, '*.+(css)'))
      .pipe(gulpNunjucks.compile({
        theme: fieldJson
      }, {
        env: require(path.join(__dirname, '..', 'env.js'))(config.src.cssDir)
      }))
      .pipe(htmlbeautify(htmlBeautifySettings))
      // output files in app folder
      .pipe(rename({
        extname: '.css'
      }))
      .pipe(jsbeautify())
      .pipe(gulp.dest(config.output.cssDir))
      .pipe(browserSync.reload({ stream: true }));

  }
};
