const path = require("path"),
  gulp = require("gulp"),
  sass = require("gulp-sass")(require('sass')),
  autoprefixer = require("gulp-autoprefixer"),
  sourcemaps = require("gulp-sourcemaps"),
  notify = require("gulp-notify"),
  beautify = require('gulp-jsbeautifier'),
  browserSync = require("browser-sync");

const config = require('./../config');
const plumber = require("gulp-plumber");
const onError = function(err) {
  notify.onError({
    title: "Gulp error in " + err.plugin,
    message: err.toString()
  })(err);
};

module.exports = {
  modules: function () {
    return gulp
      .src([path.join(config.src.moduleDir, '**/*.scss'), path.join(config.src.moduleDir, '**/*.css')])
        .pipe(sass({ /*outputStyle: "compressed"*/ }))
        .pipe(plumber({ errorHandler: onError }))
        .pipe(autoprefixer("last 5 versions"))
        .pipe(sourcemaps.write("./", { addComment: true }))
        .pipe(beautify())
        // .pipe(gulp.dest(config.src.moduleDir))
        .pipe(gulp.dest(config.output.cssDir))
        .pipe(browserSync.reload({ stream: true }));
  }
}
