const path = require("path"),
  gulp = require("gulp"),
  cleanCSS = require('gulp-clean-css');

const config = require("./../config");

module.exports = {
  minify: function () {
    return gulp
      .src([path.join(config.output.cssDir, '**/*.css'), path.join(config.output.cssDir, '*.css')])
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest(config.output.cssDir));
  }
}
