const nunjucks = require('nunjucks');
const path = require("path");

const AbstractBlock = require("../../_functions/AbstractBlock");
class DnDRow extends AbstractBlock {
  constructor(nunjucksEnv, tagName = 'dnd_row') {
    super(nunjucksEnv, tagName, __dirname);
  }
}
module.exports = DnDRow;
/*
module.exports = function() {
  this.tags = ['dnd_row'];

  this.parse = function(parser, nodes, lexer) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_dnd_row');
    parser.advanceAfterBlockEnd();

    if (args.children.length === 0) args.addChild(new nodes.Literal(0, 0, ""));

    return new nodes.CallExtension(this, 'run', args, [textContent]);
  };

  this.run = function(context, params, body) {
    let ctx = {
      content: new nunjucks.runtime.SafeString(body())
    }

    let modulePath = path.join(__dirname);
    const envModule = require('../../../env')(modulePath);

    return envModule.render(path.join(__dirname, 'index.html'), ctx);

  };
}
*/
