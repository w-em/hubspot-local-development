const nunjucks = require('nunjucks');
const path = require("path");

const AbstractBlock = require("../../_functions/AbstractBlock");
class DnDSection extends AbstractBlock {
  constructor(nunjucksEnv, tagName = 'dnd_section') {
    super(nunjucksEnv, tagName, __dirname);
  }
}
module.exports = DnDSection;

/*
module.exports = function() {
  this.tags = ['dnd_section'];

  this.parse = function(parser, nodes, lexer) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_dnd_section');
    parser.advanceAfterBlockEnd();

    let uuid = 'dnd_section-' + Date.now().toString(36) + Math.random().toString(36).substring(2);

    if (args.children.length === 0) {
      args.addChild(new nodes.KeywordArgs(0,0))
      // args.addChild(new nodes.Literal(0, 0, ""));
    }

    for (let i in args.children) {
      let child = args.children[i];
      if (child instanceof nodes.KeywordArgs) {
        child.addChild(
          new nodes.Pair(
            0,
            0,
            new nodes.Literal(0, 0, 'name'),
            new nodes.Literal(0, 0, uuid)
          ));
      }
    }

    return new nodes.CallExtension(this, 'run', args, [textContent]);
  };

  this.run = function(context, params, body) {
    let ctx = Object.assign({}, params, {
      id: params.name,
      content: new nunjucks.runtime.SafeString(body())
    });

    let modulePath = path.join(__dirname);
    const envModule = require('../../../env')(modulePath);

    return envModule.render(path.join(__dirname, 'index.html'), ctx);

  };
}
*/
