const nunjucks = require('nunjucks');
const path = require("path");
const config = require('./../../../config');
const { getFieldDefaults } = require("../../helpers");

module.exports = function() {
  this.tags = ['include_dnd_partial'];

  this.parse = function (parser, nodes, lexer) {
    // get the tag token
    var tok = parser.nextToken();

    // parse the args and move after the block end. passing true
    // as the second arg is required if there are no parentheses

    var args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(tok.value);

    // See above for notes about CallExtension
    return new nodes.CallExtension(this, 'run', args, []);
  }

  this.run = function(context, params) {

    let templatePath = path.join(config.src.templateDir, params.path);

    // const body = nunjucks.render(templatePath);
    const envModule = require('../../../env')(config.src.baseDir);

    let partialHtml = envModule.render(templatePath, {})

    params.content = new nunjucks.runtime.SafeString(partialHtml);


    var res = nunjucks.render(path.join(__dirname, 'index.html'), params);
    return res;
  };
}
