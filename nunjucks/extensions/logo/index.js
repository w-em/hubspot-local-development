const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");
/*
module.exports = function() {
  this.tags = ['logo'];

  this.parse = function(parser, nodes, lexer) {

    // get the tag token
    var tok = parser.nextToken();

    // parse the args and move after the block end. passing true
    // as the second arg is required if there are no parentheses

    var args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(tok.value);

    // See above for notes about CallExtension
    return new nodes.CallExtension(this, 'run', args, []);
  };

  this.run = function(context, params) {
    console.log(params);
    let modulePath = path.join(__dirname);
    const envModule = require('../../../env')(modulePath);

    // return nunjucks.compile(context)
    return envModule.render(path.join(__dirname, 'index.html'), {
      module: params
    });
  };
}
*/
class Logo {
  constructor(nunjucksEnv, tagName = 'logo') {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const tok = parser.nextToken();
    const args = parser.parseSignature(null, true);

    parser.advanceAfterBlockEnd(tok.value);
    return new nodes.CallExtension(this, 'run', args, null);
  }

  /**
   * @param {Object} context
   * @param {Object} [data]
   * @return {string}
   */
  run(context, data = {}) {

    let modulePath = path.join(__dirname);

    for (let i in data) {
      try {
        data[i] = this._nunjucksEnv.renderString(data[i], context.ctx)
      } catch (e) {};
    }

    const composedData = Object.assign({}, context.ctx, data);
    const envModule = require('../../../env')(modulePath);
    const renderResult = envModule.render(path.join(__dirname, 'index.html'), composedData);

    return new nunjucks.runtime.SafeString(renderResult);
  }
}
module.exports = Logo;
