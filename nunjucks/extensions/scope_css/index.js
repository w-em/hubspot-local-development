const nunjucks = require('nunjucks');
const path = require("path");
const insertPrefix = require('css-prefix');

module.exports = function() {
  this.tags = ['scope_css'];

  this.parse = function(parser, nodes, lexer) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_scope_css');
    parser.advanceAfterBlockEnd();

    if (args.children.length === 0) args.addChild(new nodes.Literal(0, 0, ""));

    return new nodes.CallExtension(this, 'run', args, [textContent]);
  };

  this.run = function(context, params, body) {
    const content = body();
    var prefixed = insertPrefix( context.ctx.name + ' .', content);
    return new nunjucks.runtime.SafeString(prefixed);
  };
}
