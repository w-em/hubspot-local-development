const nunjucks = require('nunjucks');
const path = require("path");
const AbstractBlock = require("../../_functions/AbstractBlock");
class Dnd_area extends AbstractBlock {
  constructor(nunjucksEnv, tagName = 'dnd_area') {
    super(nunjucksEnv, tagName, __dirname);
  }
}
module.exports = Dnd_area;
/*
module.exports = function() {
  this.tags = ['dnd_area'];

  this.parse = function(parser, nodes, lexer) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_dnd_area');
    parser.advanceAfterBlockEnd();

    if (args.children.length === 0) args.addChild(new nodes.Literal(0, 0, ""));

    return new nodes.CallExtension(this, 'run', args, [textContent]);
  };

  this.run = function(context, params, body) {
    params.content = typeof body === 'function' ? new nunjucks.runtime.SafeString(body()) : ''
    var res = nunjucks.render(path.join(__dirname, 'index.html'), params);

    return res;
  };
}
*/
