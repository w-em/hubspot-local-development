const nunjucks = require('nunjucks');
const path = require("path");
const AbstractBlock = require("../../_functions/AbstractBlock");
class dnd_column extends AbstractBlock {
  constructor(nunjucksEnv, tagName = 'dnd_column') {
    super(nunjucksEnv, tagName, __dirname);
  }
}
module.exports = dnd_column;
/*
module.exports = function() {
  this.tags = ['dnd_column'];

  this.parse = function(parser, nodes, lexer) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_dnd_column');
    parser.advanceAfterBlockEnd();

    if (args.children.length === 0) args.addChild(new nodes.Literal(0, 0, ""));

    return new nodes.CallExtension(this, 'run', args, [textContent]);
  };

  this.run = function(context, params, body) {
    let ctx = Object.assign({}, params, {
      id: this.uuid,
      content: new nunjucks.runtime.SafeString(body())
    });

    let modulePath = path.join(__dirname);
    const envModule = require('../../../env')(modulePath);

    return envModule.render(path.join(__dirname, 'index.html'), ctx);

  };
}
*/
