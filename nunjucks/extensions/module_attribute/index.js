const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");

class ModuleAttribute {
  constructor(nunjucksEnv, tagName = 'module_attribute') {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_module_attribute');
    parser.advanceAfterBlockEnd();

    const varName = args.children[0].value;

    return new nodes.CallExtension(this, 'run', args, [textContent], varName);
  }

  run(context, params, body, key) {
    context.ctx[key] = new nunjucks.runtime.SafeString(body());
    return ''
  }
}
module.exports = ModuleAttribute;
