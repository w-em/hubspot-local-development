const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");

class LinkedImage {
  constructor(nunjucksEnv, tagName = 'linked_image') {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const tok = parser.nextToken();
    const args = parser.parseSignature(null, true);

    parser.advanceAfterBlockEnd(tok.value);
    return new nodes.CallExtension(this, 'run', args, null);
  }

  /**
   * @param {Object} context
   * @param {Object} [data]
   * @return {string}
   */
  run(context, data = {}) {

    let modulePath = path.join(__dirname);

    for (let i in data) {
      try {
        data[i] = this._nunjucksEnv.renderString(data[i], context.ctx)
      } catch (e) {};
    }

    const composedData = Object.assign({}, context.ctx, data);
    const envModule = require('../../../env')(modulePath);
    const renderResult = envModule.render(path.join(__dirname, 'index.html'), composedData);

    return new nunjucks.runtime.SafeString(renderResult);
  }
}
module.exports = LinkedImage;
