const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");

class Form {
  constructor(nunjucksEnv, tagName = 'form') {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const tok = parser.nextToken();
    const args = parser.parseSignature(null, true);

    parser.advanceAfterBlockEnd(tok.value);
    return new nodes.CallExtension(this, 'run', args, null);
  }

  /**
   * @param {Object} context
   * @param {Object} [data]
   * @return {string}
   */
  run(context, data = {}) {

    return 'TBD: Form'
  }
}
module.exports = Form;
