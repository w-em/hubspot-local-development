const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");
const moduleFunction = require('../../_functions/moduleFunction');
const AbstractModule = require("../../_functions/AbstractModule");

class Module extends AbstractModule {
  constructor(nunjucksEnv, tagName = 'module') {
    super(nunjucksEnv, tagName, __dirname, false);
  }
}
module.exports = Module;
/*
module.exports = function() {
  this.tags = ['module'];

  this.parse = function(parser, nodes, lexer) {
    // get the tag token
    var tok = parser.nextToken();

    // parse the args and move after the block end. passing true
    // as the second arg is required if there are no parentheses

    var args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(tok.value);

    // See above for notes about CallExtension
    return new nodes.CallExtension(this, 'run', args, []);
  };

  this.run = function(context, params) {
    return moduleFunction(context, params, __dirname)
  };
}
*/
