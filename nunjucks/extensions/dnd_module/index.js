const AbstractModule = require("../../_functions/AbstractModule");

class DnDModule extends AbstractModule {
  constructor(nunjucksEnv, tagName = 'dnd_module') {
    super(nunjucksEnv, tagName, __dirname);
  }
}
module.exports = DnDModule;
