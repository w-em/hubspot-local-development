const nunjucks = require('nunjucks');
const path = require("path");
const fs = require("fs-extra");

class RequireCss {
  constructor(nunjucksEnv, tagName = 'require_css') {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const body = parser.parseUntilBlocks('end_require_css');
    parser.advanceAfterBlockEnd();

    if (args.children.length === 0) args.addChild(new nodes.Literal(0, 0, ""));

    return new nodes.CallExtension(this, 'run', args, [body]);
  }

  /**
   * @param {Object} context
   * @param {Object} [data]
   * @return {string}
   */
  run(context, data = {}, body) {

    let modulePath = path.join(__dirname);

    for (let i in data) {
      try {
        data[i] = this._nunjucksEnv.renderString(data[i], context.ctx)
      } catch (e) {};
    }

    const composedData = Object.assign({}, context.ctx, data);
    composedData.__body = body();

    const envModule = require('../../../env')(modulePath);
    const renderResult = envModule.render(path.join(__dirname, 'index.html'), composedData);

    return new nunjucks.runtime.SafeString(renderResult);
  }
}
module.exports = RequireCss;
