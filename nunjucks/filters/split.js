module.exports = function (...args) {
  const value = args[0];
  const splitCharakter = args[1];
  const limit = args[2];

  return value.split(splitCharakter, limit);
};
