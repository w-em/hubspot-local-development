const path = require("path");
module.exports.getModuleName = (module) => module.split("/").pop();

let usedFonts = {};
let themeContent = {}
let renderedFieldsContent = undefined;

function index(obj,is, value) {
  if (typeof is == 'string')
    return index(obj,is.split('.'), value);
  else if (is.length===1 && value!==undefined)
    return obj[is[0]] = value;
  else if (is.length===0)
    return obj;
  else
    return index(obj[is[0]],is.slice(1), value);
}

function parseObject( obj ) {
  const name = obj.name;
  const type = obj.type;

  let result = {}

  if (obj.default) {
    result = obj.default
  }

  if (obj.inherited_value) {
    if (obj.inherited_value.property_value_paths) {

      for (const [key, value] of Object.entries(obj.inherited_value.property_value_paths)) {
        try {
          index(result, key, index({theme: themeContent}, value));
        } catch (e) {}

      }
    }
  }

  /** font **/
  if (obj.type === 'font') {

    let fontStyle = 'font-family: "' + result.font + '"';
    if (result.fallback) {
      fontStyle = fontStyle +', ' + result.fallback;
    }
    fontStyle = fontStyle + ';\n'

    if (result.size) {
      let unit = 'px';
      if (result.size_unit) {
        unit = result.size_unit;
      }

      fontStyle = fontStyle +' ' + 'font-size: ' + result.size + unit + ';';
      result['size'] = result.size;
      result['size_unit'] = unit;
    }

    // TODO: check properties
    fontStyle = fontStyle +' ' + 'text-decoration: none;\n';

    if (parseInt(result.variant) > 0 && parseInt(result.variant) !== 400 ) {
      fontStyle = fontStyle +' ' + ' font-weight: '+ result.variant +';\n';
    } else {
      fontStyle = fontStyle +' ' + ' font-weight: normal;\n';
    }

    result['style'] = fontStyle;
    result['transform'] = 'none';

    if (result.font !== undefined) {
      if (usedFonts.hasOwnProperty(result.font) === false) {
        usedFonts[result.font] = [];
      }

      if (result.variant && usedFonts[result.font].indexOf(result.variant) < 0) {
        usedFonts[result.font].push(result.variant);
      }
    }
  }

  if (obj.type === 'border') {
    let borderStyle = '';
    for (const [key, value] of Object.entries(result)) {
      borderStyle += 'border-'+key+': ' + result[key].width.value + result[key].width.units + ' ' + result[key].style + ' ' + result[key].color + ';\n';
    }
    result['css'] = borderStyle;
  }

  if (obj.type === 'spacing') {
    let spacingStyle = '';
    for (const [key, spacing] of Object.entries(result)) {
      for (const [where, value] of Object.entries(spacing)) {
        spacingStyle += key + '-'+where+':' + value.value + value.units + ';\n'; //  + value[where].width.value + value[where].width.units + ';\n';
      }
    }
    result['css'] = spacingStyle;
  }

  if (obj.type === 'number') {
    result = obj.default;
  }

  if (obj.type === 'boolean') {
    result = JSON.parse(obj.default)
  }

  if (obj.children) {
    let cL = obj.children.length;
    for (let i = 0; i < cL;i++) {
      let c = obj.children[i];
      result[c.name] = parseObject(c);
    }
  }

  return result;
}

function getFieldDefaults (jsonArray) {
  themeContent = {}
  const len = jsonArray.length

  for (let i = 0; i < len; i++) {
    let obj = jsonArray[i];
    themeContent[obj.name] = parseObject(obj)
  }

  renderedFieldsContent = themeContent;

  return themeContent

}

module.exports.parseObject = parseObject;

module.exports.getFieldDefaults = getFieldDefaults;

module.exports.getUsedFonts = (fieldJsonData) => {
  getFieldDefaults (fieldJsonData);
  return usedFonts;
}
