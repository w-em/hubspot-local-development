function rgbToHex(r, g, b) {
  return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

module.exports = function (args, ctx) {

  const color = args[0];
  const lightness = args[1];

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);

  if (result === null) {
    return '';
  }

  const r = parseInt(result[1], 16);
  const g = parseInt(result[2], 16);
  const b = parseInt(result[3], 16);

  const changedR = Math.max(0, Math.min(r + lightness, 0xFF));
  const changedG = Math.max(0, Math.min(g + lightness, 0xFF));
  const changedB = Math.max(0, Math.min(b + lightness, 0xFF));

  return rgbToHex((changedR * 0x10 ** 4), (changedG * 0x10 ** 2), changedB);
}
