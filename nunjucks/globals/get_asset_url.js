module.exports = function (args, ctx) {
  const value = args[0];
  return value.replace(/\.\.\//ig, '');
};
