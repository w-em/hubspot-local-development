module.exports = function (args, ctx) {
  const value = args[0];
  return '<link rel="stylesheet" href="' + value + '">';
};
