/**
 * url String URL of a HubSpot-hosted image.

 width Integer (px)	The new image width, in pixels.
 height Integer (px)	The new image height, in pixels.
 length Integer (px)	The new length of the largest side, in pixels.
 upscale Boolean Use the resized image dimensions even if they would scale up the original image (images may appear blurry).Default value is false.
 upsize
 * @param args
 * @param ctx
 * @returns {*}
 */
module.exports = function (args, ctx) {
  const params = {};
  const imageUrl = args[0];
  const width = args[1];
  const height = args[2];
  const length = args[3];
  const upscale = args[4];
  const upsize = args[5];

  const newUrl = imageUrl.replace('/hubfs/', '/hs-fs/hubfs/');

  if (width > 0) params.width = width;
  if (height > 0) params.height = height;
  if (length > 0) params.length = length;
  if (upscale > 0) params.upscale = upscale;
  if (upsize > 0) params.upsize = upscale;

  const encodeDataToURL = (data) => {
    return Object
      .keys(data)
      .map(value => `${value}=${encodeURIComponent(data[value])}`)
      .join('&');
  }

  return newUrl + '?' + encodeDataToURL(params)
}
