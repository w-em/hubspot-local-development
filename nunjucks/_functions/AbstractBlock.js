const nunjucks = require("nunjucks");
const path = require("path");

class AbstractBlock {
  constructor(nunjucksEnv, tagName = 'abstract_block', dirname) {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
    this.dirname = dirname;
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const startToken = parser.nextToken();
    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(startToken.value);

    const textContent = parser.parseUntilBlocks('end_' + this.tags[0]);
    parser.advanceAfterBlockEnd();

    let uuid = this.tags[0] + '-' + Date.now().toString(36) + Math.random().toString(36).substring(2);

    if (args.children.length === 0) {
      args.addChild(new nodes.KeywordArgs(0,0))
    }

    for (let i in args.children) {
      let child = args.children[i];
      if (child.children === undefined) {
        args.children.splice(i, 1);
      }
    }

    for (let i in args.children) {
      let child = args.children[i];
      if (child instanceof nodes.KeywordArgs) {
        child.addChild(
          new nodes.Pair(
            0,
            0,
            new nodes.Literal(0, 0, 'name'),
            new nodes.Literal(0, 0, uuid)
          ));
      }
    }
    return new nodes.CallExtension(this, 'run', args, [textContent]);
  }

  /**
   * @param {Object} context
   * @param {Object} [params]
   * @param {Array} [body]
   * @return {string}
   */
  run(context, params, body) {
    let ctx = Object.assign({}, params, {
      id: params.name,
      content: typeof body === 'function' ? new nunjucks.runtime.SafeString(body()) : ''
    });

    let modulePath = path.join(this.dirname);
    let moduleSplits = modulePath.split('/');
    let moduleName = moduleSplits.pop();
    const envModule = require('../../env')(moduleSplits.join('/'));

    return envModule.render(path.join(this.dirname, 'index.html'), ctx);
  }
}

module.exports = AbstractBlock;
