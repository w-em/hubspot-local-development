const deepmerge = require('deepmerge');
const path = require("path");
const fs = require("fs-extra");
const nunjucks = require("nunjucks");

const moduleFunction = require("./moduleFunction");
const config = require("../../config");
const { getFieldDefaults } = require("../helpers");



class AbstractModule {
  constructor(nunjucksEnv, tagName = 'abstract_module', dirname, hasEnd = true) {
    this._nunjucksEnv = nunjucksEnv;
    this._cwd = null;
    this.tags = [tagName];
    this.dirname = dirname;
    this.hasEnd = hasEnd;
  }

  /**
   * @param {Object} parser
   * @param {Object} nodes
   * @return {Object}
   */
  parse(parser, nodes) {
    const tok = parser.nextToken();
    const args = parser.parseSignature(null, true);
    const uuid = this.tags[0] + '-' + Date.now().toString(36) + Math.random().toString(36).substring(2);

    parser.advanceAfterBlockEnd(tok.value);

    for (let i in args.children) {
      let child = args.children[i];
      if (child.children === undefined) {
        args.children.splice(i, 1);
      }
    }

    for (let i in args.children) {
      let child = args.children[i];
      if (child instanceof nodes.KeywordArgs) {
        child.addChild(
          new nodes.Pair(
            0,
            0,
            new nodes.Literal(0, 0, 'name'),
            new nodes.Literal(0, 0, uuid)
          ));
      }
    }

    let body;
    // parse the body  block
    if (this.hasEnd) {
      body = parser.parseUntilBlocks('end_' + this.tags[0]);

      for (let childKey in body.children) {

        let child = body.children[childKey];

        if (child.extName === 'module_attribute') {
          let lineno = args.children[0].lineno;
          let colno = args.children[0].colno;
          let varName = child.args.children[0].value;
          let name = child.args.children[0];
          body.children.splice(childKey, 1);
          let content = child.contentArgs[0].children[0].children[0].value;

          args.children[0].addChild(
            new nodes.Pair(
              lineno,
              colno,
              new nodes.Literal(lineno, colno, varName),
              new nodes.Literal(lineno, colno, content)
            ));
        }
      }

      parser.advanceAfterBlockEnd();
    }


    // console.log(content)
    return new nodes.CallExtension(this, 'run', args, [body]);
  }

  /**
   * @param {Object} context
   * @param {Object} [params]
   * @return {string}
   */
  run(context, params, body) {
/*
    console.log('--------- params -------')
    console.log(params)
*/
    const currentPath = this.dirname;
    let modulePath = path.join(params.path);
    let cleanedModulePath = modulePath.replace(/\.\/|\.\.\//g, "");
    let splittedPath = cleanedModulePath.split('/');

    switch (splittedPath[0]) {
    case '@hubspot':
      modulePath = path.join(config.hubspot, cleanedModulePath);
      break;
    case 'modules':
      modulePath = path.join(config.src.baseDir, cleanedModulePath);
      break;
    }
    if (!fs.existsSync(modulePath)) {
      if (fs.existsSync(modulePath + '.module')) {
        modulePath = modulePath + '.module';
      }
    }

    // fields.json
    const fieldFile = path.join(modulePath, 'fields.json');
    let moduleData = getFieldDefaults(JSON.parse(fs.readFileSync(fieldFile, 'utf8')));

    /*
    for (let i in moduleData) {
      if (i in params) {
        moduleData[i] = Object.assign(moduleData[i], params[i]);
      }
    }
    */
    moduleData = deepmerge(moduleData, params);

    //moduleData = Object.assign({}, moduleData, params);
/*
    console.log('--------- moduleData -------')
    console.log(fieldFile)
    console.log(moduleData)
*/
    // module.js
    const moduleJsFile = path.join(modulePath, 'module.js');
    let moduleJs = '';
    try {
      moduleJs = fs.readFileSync(moduleJsFile, 'utf8');
    } catch (e) {}


    // module.css
    const cssFile = path.join(modulePath, 'module.css');
    let moduleCss = '';
    try {
      moduleCss = fs.readFileSync(cssFile, 'utf8');
    } catch (e) {}


    if (typeof context.ctx.jsFiles === 'undefined') {
      context.ctx.jsFiles = {};
    }

    if (typeof context.ctx.cssFiles === 'undefined') {
      context.ctx.cssFiles = {};
    }

    const moduleName = modulePath.split('/').pop();

    if (typeof context.ctx.jsFiles[moduleName] === 'undefined') {
      context.ctx.jsFiles[moduleName] = moduleJs;
    } else {
      moduleJs = '';
    }

    if (typeof context.ctx.cssFiles[moduleName] === 'undefined') {
      context.ctx.cssFiles[moduleName] = moduleCss;
    } else {
      moduleCss = ''
    }

    const envModule = require('../../env')(config.baseDir);

    let unique_name = moduleName.replace('.module', '') + '-' + Date.now().toString(36) + Math.random().toString(36).substring(2);

    let moduleHtml = envModule.render(path.join(modulePath, 'module.html'), {
      module: moduleData,
      name: unique_name
    });

    params.content = new nunjucks.runtime.SafeString(moduleHtml);
    params.moduleCss = moduleCss;
    params.moduleJs = moduleJs;
    params.moduleName = moduleName;
    params.name = unique_name;

    let envWrapper  = require('../../env')(path.join(currentPath));
    var wrapper = envWrapper.render('index.html', params);

    return wrapper;
  }
}

module.exports = AbstractModule;
