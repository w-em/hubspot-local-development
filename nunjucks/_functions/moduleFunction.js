const path = require("path");
const fs = require("fs-extra");
const nunjucks = require("nunjucks");

const config = require("../../config");
const { getFieldDefaults } = require("../helpers");

module.exports = function(context, params, currentPath) {

  let modulePath = path.join(params.path);
  let cleanedModulePath = modulePath.replace(/\.\/|\.\.\//g, "");
  let splittedPath = cleanedModulePath.split('/');

  switch (splittedPath[0]) {
  case '@hubspot':
    modulePath = path.join(config.hubspot, cleanedModulePath);
    break;
  case 'modules':
    modulePath = path.join(config.src.baseDir, cleanedModulePath);
    break;
  }
  if (!fs.existsSync(modulePath)) {
    if (fs.existsSync(modulePath + '.module')) {
      modulePath = modulePath + '.module';
    }
  }

  // fields.json
  const fieldFile = path.join(modulePath, 'fields.json');
  let moduleData = getFieldDefaults(JSON.parse(fs.readFileSync(fieldFile, 'utf8')));
  for (let i in moduleData) {
    if (i in params) {
      moduleData[i] = params[i];
    }
  }

  // module.js
  const moduleJsFile = path.join(modulePath, 'module.js');
  let moduleJs = '';
  try {
    moduleJs = fs.readFileSync(moduleJsFile, 'utf8');
  } catch (e) {}


  // module.css
  const cssFile = path.join(modulePath, 'module.css');
  let moduleCss = '';
  try {
    moduleCss = fs.readFileSync(cssFile, 'utf8');
  } catch (e) {}


  if (typeof context.ctx.jsFiles === 'undefined') {
    context.ctx.jsFiles = {};
  }

  if (typeof context.ctx.cssFiles === 'undefined') {
    context.ctx.cssFiles = {};
  }

  const moduleName = modulePath.split('/').pop();

  if (typeof context.ctx.jsFiles[moduleName] === 'undefined') {
    context.ctx.jsFiles[moduleName] = moduleJs;
  } else {
    moduleJs = '';
  }

  if (typeof context.ctx.cssFiles[moduleName] === 'undefined') {
    context.ctx.cssFiles[moduleName] = moduleCss;
  } else {
    moduleCss = ''
  }

  const envModule = require('../../env')(config.baseDir);

  let moduleHtml = envModule.render(path.join(modulePath, 'module.html'), {
    module: moduleData,
    name: Date.now().toString(36) + Math.random().toString(36).substring(2)
  });

  params.content = new nunjucks.runtime.SafeString(moduleHtml);
  params.moduleCss = moduleCss;
  params.moduleJs = moduleJs;
  params.moduleName = moduleName;


  let envWrapper  = require('../../env')(path.join(currentPath));
  var wrapper = envWrapper.render('index.html', params);

  return wrapper;
}
