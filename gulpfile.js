"use strict";

const gulp = require("gulp"),
  requireDir = require('require-dir'),
  path = require('path');

const tasks = requireDir('./tasks');
const config = require('./config');

// Clean dist
gulp.task("clean", tasks.clean.dest);
gulp.task("clean:final", tasks.clean.final);
gulp.task('njk:templates', tasks.njk.templates);
gulp.task('njk:css', tasks.njk.css);
gulp.task('scss:modules', tasks.scss.modules);
gulp.task('js:main', tasks.javascript.main);
gulp.task('js:modules', tasks.javascript.modules);
gulp.task('images', tasks.images);
gulp.task('serve', tasks.serve);
gulp.task('copy:final', tasks.copy.final);
gulp.task('css:minify', tasks.css.minify);

gulp.task("gulp-watch", function() {
  gulp.watch([
    path.join(config.src.templateDir, '*.+(html|njk)'),
    path.join(config.src.templateDir, '**/*.+(html|njk)'),
    path.join(config.src.moduleDir, '**/*.html'),
    path.join(config.src.moduleDir, '**/fields.json'),
  ], gulp.series('njk:templates'));

  gulp.watch([
    path.join(config.src.cssDir, '*.css'),
    path.join(config.src.cssDir, '**/*.css'),
  ], gulp.series('njk:css'));

  gulp.watch([path.join(config.src.jsDir, '*.js')], gulp.series('js:main'));

  gulp.watch([path.join(config.src.moduleDir, '**/*.js')], gulp.series('js:modules'));

  gulp.watch([
    path.join(config.src.moduleDir, '**/*.scss'),
    path.join(config.src.moduleDir, '**/*.css')
  ], gulp.series('scss:modules'));

  gulp.watch([path.join(config.src.imageDir, '**/*')], gulp.series('images'));

  gulp.watch([path.join(config.src.baseDir, 'fields.json')], gulp.series('njk:css', 'njk:templates'));
});

gulp.task("build", gulp.series(
  gulp.parallel(
    'clean',
    'njk:templates',
    'njk:css',
    'js:main',
    'js:modules',
    'scss:modules',
    'images'
  ),
  gulp.parallel(
    'css:minify'
  )
));

gulp.task("final", gulp.series(
  gulp.parallel(
    'clean',
    'clean:final',
  ),
  gulp.parallel('copy:final')
));

gulp.task("watch", gulp.series(gulp.parallel("gulp-watch", "serve")));

gulp.task("default", gulp.series("build", gulp.parallel("gulp-watch", "serve")));
