const nunjucks = require("nunjucks");
const { readdirSync, readFileSync } = require('fs')
const path = require("path");
const config = require('./config');

nunjucks._globals = require('nunjucks/src/globals.js');

// give access to globals through a global
nunjucks._globals.globals = function() { return nunjucks._globals }

const getDirectories = source => readdirSync(source, { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())
  .map(dirent => dirent.name)

const extensionDirectory = path.join(__dirname, './nunjucks/extensions');
const globalDirectory = path.join(__dirname, './nunjucks/globals');
const filterDirectory = path.join(__dirname, './nunjucks/filters');

const { getFieldDefaults, getUsedFonts } = require("./nunjucks/helpers");
const fieldFile = path.join(config.src.baseDir, 'fields.json');
const fieldJsonData = JSON.parse(readFileSync(fieldFile, 'utf8'));
const fieldJson = getFieldDefaults(fieldJsonData);
const usedFonts = getUsedFonts(fieldJsonData);

let environmentNames = []
let environments = []

function env(tmplDir, noCache = false) {
  let index = environmentNames.indexOf(tmplDir)
  if (index > -1) {
    return environments[index];
  } else {
    const env = createEnv(tmplDir, noCache);
    environmentNames.push(tmplDir);
    environments.push(env);
    return env;
  }
}

function createEnv (tmplDir, noCache = false) {
  /**
   * Environment
   */
  nunjucks.installJinjaCompat();

  const nunjucksFileLoader = new nunjucks.FileSystemLoader(tmplDir, {
    noCache: noCache,
  });
  let env = new nunjucks.Environment(nunjucksFileLoader, {
    autoescape: false,
    noCache: noCache
  });

  // install extensions
  const extensions = getDirectories(extensionDirectory);
  for (let ext in extensions) {
    let extName = extensions[ext];
    let extFunction = require(path.join(extensionDirectory, extName));
    env.addExtension(extName, new extFunction(env));
  }

  // install filters
  readdirSync(filterDirectory).forEach(file => {
    let fileName = file.substr(0, file.length - 3);
    let filterFunction = require(path.join(filterDirectory, file));
    env.addFilter(fileName, filterFunction);
  });

  // install globals
  readdirSync(globalDirectory).forEach(file => {
    let fileName = file.substr(0, file.length - 3);
    let globalFunction = require(path.join(globalDirectory, file));
    env.addGlobal(fileName, function (...args) {
      return globalFunction(args, this.ctx)
    });
  });

  env.addGlobal('standard_header_includes', function () {
    let fontsArr = []
    for (let key in usedFonts) {
      fontsArr.push(key + ':' + usedFonts[key].join(','));
    }

    let googleFonts = '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=' + fontsArr.join('|') + '&amp;display=swap">'

    return googleFonts + '<meta name="viewport" content="width=device-width, initial-scale=1">';
  }());

  return env;
}

module.exports = env
